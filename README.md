# FunLanguage: a functional language written in OCaml #

## Final project for the LIP 2017/2018 course (University of Cagliari) ##

### Developed by: Mario Taccori (mario.taccori@outlook.com), Edoardo Cittadini (edo.citta@gmail.com) ###

#### The language specifications pdf lists the specifications for the language given by the teacher. ####
#### The language design choices describes the design choices we made to achieve a better result. ####


## Final grade: 6.49/6.49 ##